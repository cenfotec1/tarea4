#include "../include/RegistrosUsuarios.hpp"

void RegistrosUsuarios::addUsuario(const Usuario& usuario) {
  usuarios.push_back(usuario);
}

void RegistrosUsuarios::eliminarUsuario(const Usuario& usuario) {
  usuarios.remove(usuario);
}

void RegistrosUsuarios::verUsuarios() {
  for (std::list<Usuario>::iterator it = usuarios.begin(); it != usuarios.end(); ++it) {
      std::cout << "Name: " << it->getName() << ", ";
      std::cout << "ID: " << it->getId() << ", ";
      std::cout << "Phone: " << it->getPhone() << "\n";
  }
}
