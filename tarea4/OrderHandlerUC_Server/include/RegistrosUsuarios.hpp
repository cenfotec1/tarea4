#ifndef REGISTROSUSUARIOS_HPP
#define REGISTROSUSUARIOS_HPP

#include <iostream>
#include <list>
#include <iterator>
#include "Usuario.hpp"

class RegistrosUsuarios {
  private:
    std::list<Usuario> usuarios;
    enum usuario_error { usuario_agregado, usuario_existe, usuario_no_existe, };
  public:
    void addUsuario(const Usuario& usuario);
    void eliminarUsuario(const Usuario& usuario);
    void verUsuarios();
};

#endif
