#ifndef USUARIO_HPP
#define USUARIO_HPP

#include <iostream>
#include <string>

class Usuario {
  private:
    std::string name;
    std::string id;
    std::string phone;
    std::string password;

  public:
    Usuario(std::string name, std::string id, std::string phone, std::string password);
    std::string getName();
    void setName(std::string name);
    std::string getId();
    void setId(std::string id);
    std::string getPhone();
    void setPhone(std::string phone);
    std::string getPassword();
    void setPassword(std::string password);
    bool operator==(const Usuario& u) const;
};
#endif
