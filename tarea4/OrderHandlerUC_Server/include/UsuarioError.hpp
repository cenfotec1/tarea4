#pragma once

enum class UsuarioError {
  usuario_agregado,
  usuario_existe,
  usuario_no_existe
};
