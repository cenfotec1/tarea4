#include "../include/Usuario.hpp"

Usuario::Usuario(std::string name, std::string id, std::string phone, std::string password) {
  this->name = name;
  this->id = id;
  this->phone = phone;
  this->password = password;
}

std::string Usuario::getName() {
  return name;
}

void Usuario::setName(std::string name) {
  this->name = name;
}

std::string Usuario::getId() {
  return id;
}

void Usuario::setId(std::string id) {
  this->id = id;
}

std::string Usuario::getPhone() {
  return phone;
}

void Usuario::setPhone(std::string phone) {
  this->phone = phone;
}

std::string Usuario::getPassword() {
  return password;
}

void Usuario::setPassword(std::string password) {
  this->password = password;
}

bool Usuario::operator==(const Usuario& u) const {
    return (name == u.name && id == u.id && phone == u.phone && password == u.password);
}
