#include <iostream>

#include "../include/Usuario.hpp"
#include "../include/RegistrosUsuarios.hpp"

int main() {
  Usuario usuario_1("Marco", "346662677", "5553-1234", "password123");
  Usuario usuario_2("Luis", "124130454", "8511-6522", "password12344");
  Usuario usuario_3("Kevin", "109130456", "8888-6528", "password1211");

  RegistrosUsuarios registros_usuarios;
  registros_usuarios.addUsuario(usuario_1);
  registros_usuarios.addUsuario(usuario_2);
  registros_usuarios.addUsuario(usuario_3);

  registros_usuarios.verUsuarios();
  std::cout << "" << std::endl;
  registros_usuarios.eliminarUsuario(usuario_1);
  registros_usuarios.verUsuarios();
  return 0;
}
