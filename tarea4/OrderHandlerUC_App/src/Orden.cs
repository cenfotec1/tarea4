using System;

namespace Orders
{
  public class Orden {
    private int id;
    private string? name;
    private double price;
    private string orderDate;

    public Orden(int id, string? name, double price)
    {
      this.id = id;
      this.name = name;
      this.price = price;

      DateTime fecha = DateTime.Now;
      this.orderDate = fecha.ToString("D", new System.Globalization.CultureInfo("es-ES"));
    }

    public int Id   // property
    {
      get { return id; }   // get method
      set { id = value; }  // set method
    }
    public string Name   // property
    {
      get { return name; }   // get method
      set { name = value; }  // set method
    }
    public double Price   // property
    {
      get { return price; }   // get method
      set { price = value; }  // set method
    }
    public string OrderDate   // property
    {
      get { return orderDate; }   // get method
      set { orderDate = value; }  // set method
    }
  }


}