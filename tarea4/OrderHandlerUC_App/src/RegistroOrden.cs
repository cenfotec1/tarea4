using System;
using System.Collections.Generic;

namespace Orders
{
  /*
      Clase para la lógica de creación, visualización y registro de órdenes.
  */
  public class RegistroOrden
  {
    private List<Orden> ordenes = new List<Orden>();

    /*
        Crea una nueva orden por medio de entrada de consola y la agrega al registro de ordenes.
    */
    public void agregarOrden()
    {
      Console.WriteLine("Indique el ID de la orden");
      int id = Convert.ToInt32(Console.ReadLine());

      Console.WriteLine("Indique el precio de la orden");
      double precio = Convert.ToDouble(Console.ReadLine());
      
      Console.WriteLine("Indique el nombre de su orden");
      string? nombre = Console.ReadLine();

      Orden orden_registrar = new Orden(id, nombre, precio);
      ordenes.Add(orden_registrar);
    }

    /*
        Imprime todas las órdenes actuales
    */
    public void verOrdenes()
    {
      foreach(var ordenTemporal in ordenes)
      {
        Console.WriteLine($"ID: {ordenTemporal.Id}; Name: {ordenTemporal.Name}; Precio: ${ordenTemporal.Price}; Fecha: {ordenTemporal.OrderDate} ");
      }
    }

    /*
        Elimina una orden por ID
    */
    public void eliminarOrden(int id)
    { 
      foreach(Orden ordenTemporal in ordenes)
      {
        if(ordenTemporal.Id == id)
        {
          int index = ordenes.IndexOf(ordenTemporal);
          ordenes.RemoveAt(index);
          return;
        }
      }
      Console.WriteLine("No se halló orden a eliminar");
      return;
    }
    public List<Orden> Ordenes
    {
      get {return ordenes;}
    }
  }
}
