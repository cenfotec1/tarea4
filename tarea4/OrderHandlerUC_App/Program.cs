﻿using System;

using Orders;

internal class Program
{
    private static void Main(string[] args)
    {
        RegistroOrden registro_ordenes = new RegistroOrden();
        bool menu_on = true;
        int opcion;
        int id_eliminar;
        while (menu_on)
        {
            Console.WriteLine("Eliga una opción: ");
            Console.WriteLine("1: Agregar una orden \n2: Ver órdenes \n3: Eliminar una orden \nOtro número: Salir");
            opcion = Convert.ToInt32(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    registro_ordenes.agregarOrden();
                    break;
                case 2:
                    registro_ordenes.verOrdenes();
                    break;
                case 3:
                    Console.WriteLine("Eliga ID de orden a eliminar");
                    id_eliminar = Convert.ToInt32(Console.ReadLine());
                    registro_ordenes.eliminarOrden(id_eliminar);
                    break;
                default:
                    menu_on = false;
                    break;
            }
            
        }
    }
}
